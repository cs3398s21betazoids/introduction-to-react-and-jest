import React from 'react';
import calculateWinner from './calculateWinner.js'

/*
	Winning tic tac toe squares (when each square has same symbol)
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
 */

//  testsquare indices = [0,1,2,3,4,5,6,7,8]

test('Expect Winner to be X, left-to-right diagonal down', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[0] = 'X';
	testsquares[4] = 'X';
	testsquares[8] = 'X';
  	expect(calculateWinner(testsquares)).toBe('X');
});